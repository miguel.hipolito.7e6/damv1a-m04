<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>   
 
<xsl:template match="/ies">
    <h1><xsl:value-of select="@nombre"/></h1>
    <p>Pagina web: 
        <a>
            <xsl:attribute name="href">
                <xsl:value-of select="@web"/>
            </xsl:attribute>
            <xsl:value-of select="@web"/>
        </a>
    </p>
    <table style="border: 1px solid black">
        <tr>
            <th>Nombre del ciclo</th>
            <th>Grado</th>
            <th>Año del titulo</th>
        </tr>
        <xsl:apply-templates select="ciclos"/>
    </table>
</xsl:template>

<xsl:template match="ciclos">
    <xsl:for-each select="ciclo" >
        <xsl:sort select="decretoTitulo/@año" order="descending"></xsl:sort>
        <tr>
            <td><xsl:value-of select="nombre"/></td>
            <td><xsl:value-of select="grado"/></td>
            <td><xsl:value-of select="decretoTitulo/@año"/></td>
        </tr>
    </xsl:for-each>
    
</xsl:template>
    
</xsl:stylesheet>
