<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes"/> 

<xsl:variable name="idmuse" select="/discos/group[1]/name" /> <!-- variables fuera o dentro-->
<xsl:variable name="idfeeder" select="/discos/group[2]/name" /> <!--puede tener path o valor-->

<xsl:template match="/discos">
    <lista>
        <xsl:apply-templates/>
    </lista>
</xsl:template>

<xsl:template match="group">
    <!-- o ver cada grupo, guardar el name en una variable, y comprobar cada disco -->
</xsl:template>

<xsl:template match="disco">
    <disco>
        <xsl:choose>
            <xsl:when test="interpreter/@id = 'museId'"> 
                <xsl:value-of select="title"></xsl:value-of> 
                es interpretado por:
                <xsl:value-of select="$idmuse"></xsl:value-of> 
            </xsl:when>

            <xsl:otherwise>
                <xsl:value-of select="title"></xsl:value-of> 
                es interpretado por:
                <xsl:value-of select="$idfeeder"></xsl:value-of>
            </xsl:otherwise>
        </xsl:choose>
    </disco>
</xsl:template>
</xsl:stylesheet>