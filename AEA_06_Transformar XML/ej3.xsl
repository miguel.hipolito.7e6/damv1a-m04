<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>   
 
<xsl:template match="/">
    <html>
        <body> 
            <table style="border: 1px solid blue">
                <tr style="text-align:center">
                    <td>Nombre</td>
                    <td>Apellidos</td>
                    <td>Telefono</td>
                    <td>Repetidor</td>
                    <td>Nota practica</td>
                    <td>Nota examen</td>
                    <td>Nota final</td>
                    <td>Foto</td>
                </tr>
                <xsl:apply-templates/>
            </table>
         </body>
    </html>
</xsl:template>

<xsl:template match="evaluacion"> <!-- primero hazlo entero, luego modulariza (copiando y pegando a partir del for) -->
    <xsl:for-each select="alumno"> <!-- evitar . como path? -->
    <xsl:sort select="apellidos" />
        <tr>
            <td><xsl:value-of select="nombre"></xsl:value-of></td>
            <td><xsl:value-of select="apellidos"></xsl:value-of></td>
            <td><xsl:value-of select="telefono"></xsl:value-of></td>
            <td><xsl:value-of select="@repetidor"></xsl:value-of></td>
            <td><xsl:value-of select="notas/practicas"></xsl:value-of></td>
            <td><xsl:value-of select="notas/examen"></xsl:value-of></td>
            
            <xsl:choose>
                <xsl:when test="(notas/practicas + notas/examen) div 2 &lt; 5">
                    <td style="background-color: red;"><xsl:value-of select="(notas/practicas + notas/examen) div 2"></xsl:value-of></td>
                </xsl:when>
                <xsl:when test="(notas/practicas + notas/examen) div 2 &gt;= 8">
                    <td style="background-color: blue;"><xsl:value-of select="(notas/practicas + notas/examen) div 2"></xsl:value-of></td>
                </xsl:when>
                <xsl:otherwise>
                    <td><xsl:value-of select="(notas/practicas + notas/examen) div 2"></xsl:value-of></td>
                </xsl:otherwise>
            </xsl:choose>

            <td>
                <img>
                    <xsl:choose>
                        <xsl:when test="foto"> <!-- si foto existe, o tambien con @foto-->
                            <xsl:attribute name="src">src/avatar.jpg</xsl:attribute>
                            <xsl:attribute name="width">30</xsl:attribute>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:attribute name="src">src/defecto.jpg</xsl:attribute>
                            <xsl:attribute name="width">30</xsl:attribute>
                        </xsl:otherwise>
                    </xsl:choose>
                </img>
            </td>
        </tr>
    </xsl:for-each>
</xsl:template>

</xsl:stylesheet>
