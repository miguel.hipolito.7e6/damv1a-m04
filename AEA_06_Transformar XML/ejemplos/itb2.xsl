<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes"/>    
<xsl:template match="/itb">
   <html>
       <body>
        <xsl:for-each select="aula">
            <h1>El aula es: <xsl:value-of select="@num"></xsl:value-of></h1>
                <p>Mis alumnos son:</p>
                <ul>
                    <xsl:for-each select="alumno">
                        <xsl:sort select="nombre"/>
                        <li><xsl:value-of select="nombre"></xsl:value-of></li>
                    </xsl:for-each>
                </ul>
        </xsl:for-each>
       </body>
    </html>
            <!-- <xsl:for-each select="canciones/archivo">
                [cancion:<xsl:value-of select="cancion"></xsl:value-of>,
                artista:<xsl:value-of select="artista"></xsl:value-of>,
                disco:<xsl:value-of select="disco"></xsl:value-of>]
            
            </xsl:for-each> -->
        
    </xsl:template>
</xsl:stylesheet>