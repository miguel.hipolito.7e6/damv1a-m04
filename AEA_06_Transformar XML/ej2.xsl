<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>   
 
<xsl:template match="/root">
    <html>
        <body> 
            <table style="border: 1px solid blue">
                <tr style="text-align:center">
                    <td>Fecha</td>
                    <td>Maxima</td>
                    <td>Minima</td>
                    <td>Prediccion</td>
                </tr>
                <xsl:apply-templates/>
            </table>
         </body>
    </html>
</xsl:template>
    
<xsl:template match="prediccion">
    <xsl:for-each select="dia">
    <xsl:sort select="temperatura/maxima" order="descending"/> 
        <tr style="text-align:center">
            <td><xsl:value-of select="@fecha"></xsl:value-of></td>
            <td><xsl:value-of select="temperatura/maxima"></xsl:value-of></td>
            <td><xsl:value-of select="temperatura/minima"></xsl:value-of></td>
            <td>
            <xsl:value-of select="estado_cielo[@periodo='00-24']/@descripcion"></xsl:value-of>
            <img src="{concat('imagenes/',estado_cielo[@periodo='00-24']/@descripcion)}.png" width="100px"/> <!--tiene que tener el mismo nombre las imagenes, es el concat?-->
            </td>
        </tr>
    </xsl:for-each>
</xsl:template>
</xsl:stylesheet>

<!--Primero instalar xsltproc, luego ejecutar el xsltproc entrada.sxl entrada.xml > salida.html-->