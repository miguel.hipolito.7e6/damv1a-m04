<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes"/>   
 
<xsl:template match="/programacio">
    <cadena>
        <nom>Un Tv</nom>
        <programas>
            <xsl:apply-templates select="audiencia"/>
        </programas>
    </cadena>
</xsl:template>

<xsl:template match="audiencia">
<programa>
    <xsl:attribute name="hora">
        <xsl:value-of select="hora"></xsl:value-of>
    </xsl:attribute>
    <xsl:apply-templates select="cadenes"/>
</programa>
</xsl:template>

<xsl:template match="cadenes">
    <nom-programa><xsl:value-of select="cadena[2]/text()"></xsl:value-of></nom-programa>
    <audiencia><xsl:value-of select="cadena[2]/@percentatge"></xsl:value-of></audiencia>
</xsl:template>
    
</xsl:stylesheet>
